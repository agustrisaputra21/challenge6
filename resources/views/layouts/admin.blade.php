<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <!-- Head -->
    @include('admin.includes.head')

    <!-- Styles -->
    @include('admin.includes.style')
</head>
<body class="hold-transition skin sidebar-mini">
    <div class="wrapper">
        <!-- Main Header -->
        @include('admin.includes.header')
        <!-- End Of Main Header -->

        <!-- Side Menu -->
        <aside class="main-sidebar">
            @include('admin.includes.sidebar')
        </aside>
        <!-- End Of Side Menu -->

        <!-- Content Wrapper-->
        <div class="content-wrapper">
            <!-- Content Header -->
            <section class="content-header">
                @yield('content-header')
            </section>

            <!-- Main Content -->
            <section class="content">
                @yield('content')
            </section>
        </div>
        <!-- End Of Content -->

        <!-- Footer -->
        <footer class="main-footer">
            @include('admin.includes.footer')
        </footer>
        <!-- End Of Footer -->
    </div>

    @yield('modal')

    <!-- Script -->
    @include('admin.includes.script')
    <!-- End -->

    @yield('js')
</body>
</html>