
    <div class="modal fade" id="boardModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel"></h4>
                </div>
                <div class="modal-body pad-20">
                    <div class="alert alert-danger alert-dismissible" role="alert" id="delete-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <p id="error_msg"></p>
                    </div>
                    <form id="bulletinForm" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="id" id="id" value="">
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" class="form-control" name="board_name" id="board_name_edit" value="">
                        </div>
                        <div class="form-group">
                            <label>Title</label>
                            <input type="text" class="form-control" id="board_title_edit" name="board_title" value="">
                        </div>
                        <div class="form-group">
                            <label>Body</label>
                            <textarea rows="5" class="form-control" id="board_body_edit" name="board_body"></textarea>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-4" id="board_image" name="board_image">   
                            </div>
                            <div class="col-md-8 pl-0" id="board_edit_img" style="display: none">
                                <label>Choose image from your computer :</label>
                                <div class="input-group">
                                    <input type="text" class="form-control upload-form" value="No file chosen" readonly>
                                    <span class="input-group-btn">
                                        <span class="btn btn-default btn-file">
                                            <i class="fa fa-folder-open"></i>&nbsp;Browse <input type="file" id="board_img_edit" name="board_img" multiple>
                                        </span>
                                    </span>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="img_checked">Delete image
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group" id="pwd" style="display: none"> 
                        </div>
                    </form>
                    <div class="alert alert-warning alert-dismissible" role="alert" id="delete-warning" style="display:none">
                        <p>Are you sure want to delete this item?</p>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>