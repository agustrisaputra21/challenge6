<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <title>Timedoor Challenge - Level 8</title>

        <!-- Style -->
        <!-- CSS -->
        <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('css/font-awesome.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('css/tmdrPreset.css')}}">
        <!-- CSS End -->

        <!-- script -->
        <!-- Javascript -->
        <script type="text/javascript" src="{{asset('js/jquery.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
        <!-- Javascript End -->

    </head>

    <style>
        .error {color: #FF0000;}
    </style>

    <body class="bg-lgray">
        <header>
            @include('bulletin.template.header')
        </header>

        <main>
            @yield('section')
        </main>
        
        <footer>
            @include('bulletin.template.footer')
        </footer>
    
        @include('bulletin.bulletin_modal')
    </body>

    <script type="text/javascript" src="{{ asset('js/bulletin.js') }}"></script>
    <script>
        // INPUT TYPE FILE
        $(document).on('change', '.btn-file :file', function() {
            var input = $(this),
            numFiles = input.get(0).files ? input.get(0).files.length : 1,
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [numFiles, label]);
        });

        $(document).ready( function() {
            $('.btn-file :file').on('fileselect', function(event, numFiles, label) {
                var input = $(this).parents('.input-group').find(':text'),
                log = numFiles > 1 ? numFiles + ' files selected' : label;

                if( input.length ) {
                    input.val(log);
                } else {
                    if( log ) alert(log);
                }
            });
        });

        $(document).on('ajaxComplete ready', function () {
            $('.boardModal').off('click').on('click', function () {
                $('#modalMdContent').load($(this).attr('value'));
                $('#modalMdTitle').html($(this).attr('title'));
            });
        });

        $('#boardModal').on('hidden.bs.modal', function (e) {
            $(this)
                // .find("input")
                //     .val('')
                //     .end()
                .find("input[type=text]")
                    .val('No file chosen')
                    .end()
                .find("input[type=checkbox]")
                    .prop("checked", "")
                    .end();
        });

        function editById(id) {
            var form_data = new FormData();

            form_data.append('_token', "{{ csrf_token() }}");
            form_data.append('id', id);
            form_data.append('board_name', $('#board_name_edit').val());
            form_data.append('board_title', $('#board_title_edit').val());
            form_data.append('board_body', $('#board_body_edit').val());
            form_data.append('img_checked', $('input[name=img_checked]').prop('checked'));
    
            if($('#board_img_edit')[0].files[0] !== undefined) {
                form_data.append('board_img', $('#board_img_edit')[0].files[0]);
            } else {
                form_data.append('board_img', '');
            }
    
            $('#delete-danger').html(``)

            $.ajax({
                type: 'post',
                data: form_data,
                cache: false,
                contentType: false,
                processData: false,
                url: 'board-edit',
                success: function(data) {
                    if(data.success) {
                        window.location.replace('/')
                    }
                },
                error: function(data) {
                    if( data.status === 422 ) {
                        var errors = data.responseJSON.errors;
                        $.each(errors, function (_key, value) {
                            $('#delete-danger').show().append(value+"<br/>");
                        });
                    }
                }
            });
        }

        function deleteById(id) {
            $.ajax({
                type: 'delete',
                data: $('#bulletinForm').serialize(),
                url: 'board/' + id,
                success: function(data) {
                    if(data.success) {
                        window.location.replace('/');
                    }
                }
            });
        }

    </script>
</html>