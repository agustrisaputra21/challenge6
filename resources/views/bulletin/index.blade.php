@extends('bulletin.template.layout')

@section('section')
    <div class="section">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 bg-white p-30 box">
                    <div class="text-center">
                        <h1 class="text-green mb-30"><b>Level 8 Challenge</b></h1>
                    </div>
                    @if (session('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button> 
                            <strong>{{ session('success') }}</strong>
                        </div>
                    @endif

                    @if (session('error'))
                        <div class="alert alert-danger alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button> 
                            <strong>{{ session('error') }}</strong>
                        </div>
                    @endif

                    <form action="{{'board'}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label>Name</label>
                            @guest
                                <input type="text" class="form-control" name="board_name" value="{{ old('board_name') }}">
                                @if( $errors->has('board_name') )
                                    <span class="error">{{ $errors->first('board_name') }}</span>
                                @endif
                            @else
                                <input type="text" class="form-control" name="board_name" value="{{ Auth::user()->name }}">
                            @endguest
                        </div>
                        <div class="form-group">
                            <label>Title</label>
                            <input type="text" class="form-control" name="board_title" value="{{ old('board_title') }}">
                            @if( $errors->has('board_title') )
                                <span class="error">{{ $errors->first('board_title') }}</span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Body</label>
                            <textarea rows="5" class="form-control" name="board_body">{{ old('board_body') }}</textarea>
                            @if( $errors->has('board_body') )
                                <span class="error">{{ $errors->first('board_body') }}</span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Choose image from your computer :</label>
                            <div class="input-group">
                                <input type="text" class="form-control upload-form" value="No file chosen" readonly>
                                <span class="input-group-btn">
                                    <span class="btn btn-default btn-file">
                                        <i class="fa fa-folder-open"></i>&nbsp;Browse <input type="file" name="board_img" multiple>
                                    </span>
                                </span>
                            </div>
                            @if( $errors->has('board_img') )
                                <span class="error">{{ $errors->first('board_img') }}</span>
                            @endif
                        </div>
                        @guest
                            <div class="form-group">
                                <label>Password</label>
                                <input type="password" class="form-control" name="password">
                                @if( $errors->has('password') )
                                    <span class="error">{{ $errors->first('password') }}</span>
                                @endif
                            </div>
                        @endguest
                        <div class="text-center mt-30 mb-30">
                            <button class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                    <hr>

                    @foreach($boards as $board)
                        <div class="post">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h2 class="mb-5 text-green"><b>{{$board->title}}</b></h2>
                                </div>
                                <div class="pull-right text-right">
                                    <p class="text-lgray">{{date('d-m-Y', strtotime($board->created_at))}}
                                        <br>
                                        <span class="small">{{date('H:i', strtotime($board->created_at))}}</span>
                                    </p>
                                </div>
                            </div>
                            <h4 class="mb-20">{{ $board->name ? $board->name : 'No Name' }} <span class="text-id">- {{ $board->user_id ? '[ID:' . $board->user_id . ']' : ''}}</span></h4>
                            <p>{!! nl2br(e($board->body)) !!}</p>
                            @if($board->img)
                                <div class="img-box my-10">
                                    <img class="img-responsive img-post" src="{{ asset($board->imagePath) }}" alt="image">
                                </div>
                            @else
                                <div class="img-box my-10">
                                    <img class="img-responsive img-post" src="http://via.placeholder.com/300x300" alt="image">
                                </div>
                            @endif
 
                            <form class="form-inline mt-50" id="bulletinForm{{$board->id}}">
                                @csrf
                                <input type="hidden" name="id" value="{{$board->id}}">
                                @if(Auth::user())
                                    @if(Auth::user()->id == $board->user_id)
                                        <a type="submit" class="btn btn-default mb-2" data-toggle="modal" data-target="#editModal" onclick="editBulletin({{$board->id}})"><i class="fa fa-pencil p-3"></i></a>
                                        <a type="submit" class="btn btn-danger mb-2" data-target="#deleteModal" onclick="deleteBulletin({{$board->id}})"><i class="fa fa-trash p-3"></i></a>
                                    @endif
                                @else
                                    @empty($board->user_id)
                                        <div class="form-group mx-sm-3 mb-2">
                                            <label for="inputPassword2" class="sr-only">Password</label>
                                            <input type="password" class="form-control" name="password" placeholder="Password">
                                        </div>
                                        <a type="submit" class="btn btn-default mb-2" data-toggle="modal" data-target="#editModal" onclick="editBulletin({{$board->id}})"><i class="fa fa-pencil p-3"></i></a>
                                        <a type="submit" class="btn btn-danger mb-2" data-target="#deleteModal" onclick="deleteBulletin({{$board->id}})"><i class="fa fa-trash p-3"></i></a>
                                    @endempty
                                @endif
                            </form>
                        </div>
                    @endforeach

                    <div class="text-center mt-30">
                        <nav>
                            <ul class="pagination">
                                {{ $boards->links() }}
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection