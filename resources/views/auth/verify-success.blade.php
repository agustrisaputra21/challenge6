@extends('layouts.auth')

@section('title') Verified @endsection

@section('content')
<div class="box login-box text-center">
    <div class="login-box-body">
        <p>Thank you for your registration. Membership is now complete.</p>
    </div>
    <div class="login-box-footer">
        <div class="text-center">
        <a href="{{'/'}}" class="btn btn-primary">Back to Home</a>
        </div>
    </div>
</div>
@endsection
