@extends('layouts.auth')

@section('title') Register Confirm @endsection

@section('content')
<div class="box login-box">
    <div class="login-box-head">
        <h1>Register</h1>
    </div>
    <div class="login-box-body">
        <table class="table table-no-border">
            <tbody>
                <tr>
                    <th>Name</th>
                    <td>{{$request->name}}</td>
                </tr>
                <tr>
                    <th>E-mail</th>
                    <td>{{$request->email}}</td>
                </tr>
                <tr>
                    <th>Password</th>
                    <td>{{$request->password}}</td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="login-box-footer">
        <div class="text-right">
            <form method="POST" action="{{ route('register') }}">
                @csrf
                <div class="form-group hide">
                    <input type="text" class="form-control" placeholder="Name" name="name" value="{{ $request->name }}">
                </div>
                <div class="form-group hide">
                    <input type="email" class="form-control" placeholder="E-mail" name="email" value="{{ $request->email }}">
                </div>
                <div class="form-group hide">
                    <input type="password" class="form-control" placeholder="Password" name="password" value="{{ $request->password }}">
                </div>
                <a href="javascript:history.back()" class="btn btn-default">Back</a>
                <button type="submit" class="btn btn-primary">
                    {{ __('Confirm') }}
                </button>
            </form>
        </div>
    </div>
</div>
@endsection
