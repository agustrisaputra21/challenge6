@extends('layouts.auth')

@section('title') Register @endsection

@section('content')
<div class="box login-box">
    <form method="POST" action="{{ route('confirm') }}">
        @csrf
        <div class="login-box-head">
            <h1 class="mb-5">{{ __('Register') }}</h1>
            <p class="text-lgray">Please fill the information below...</p>
        </div>
        <div class="login-box-body">    
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Name" name="name" value="{{ old('name') }}">
                @error('name')
                    <span class="invalid-feedback text-danger" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="form-group">
                <input type="email" class="form-control" placeholder="E-mail" name="email" value="{{ old('email') }}">
                @error('email')
                    <span class="invalid-feedback text-danger" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="form-group">
                <input type="password" class="form-control" placeholder="Password" name="password">
                @error('password')
                    <span class="invalid-feedback text-danger" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
        <div class="login-box-footer">
            <div class="text-right">
                <a href="{{'/'}}" class="btn btn-default">Back</a>
                <button type="submit" class="btn btn-primary">
                    {{ __('Confirm') }}
                </button>
            </div>
        </div>
    </form>
</div>
@endsection
