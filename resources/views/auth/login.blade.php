@extends('layouts.auth')

@section('title') Login @endsection

@section('content')
<div class="box login-box">
    <form method="POST" action="{{ route('login') }}">
        @csrf
        <div class="login-box-head">
            <h1 class="mb-5">{{ __('Login') }}</h1>
            <p class="text-lgray">Please login to continue...</p>
        </div>
        @if (session('error'))
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button> 
                <p>{{ session('error') }}</p>
            </div>
        @endif
        <div class="login-box-body">
            <div class="form-group">
                <input type="email" class="form-control" placeholder="Email" name="email" value="{{ old('email') }}">
                @error('email')
                    <p class="mt-5 small text-danger">*this field can't be empty</p>
                @enderror
            </div>
            <div class="form-group">
                <input type="password" class="form-control" placeholder="Password" name="password">
                @error('password')
                    <p class="mt-5 small text-danger">*this field can't be empty</p>
                @enderror
            </div>
        </div>
        <div class="login-box-footer">
            <div class="text-right">
                <button type="submit" class="btn btn-primary">
                    {{ __('Submit') }}
                </button>
            </div>
        </div>
    </form>
</div>
@endsection
