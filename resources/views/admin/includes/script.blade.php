    <!-- jQuery 3 -->
    <script src="{{ asset('assets/plugin/jquery/jquery.js') }}"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="{{ asset('assets/plugin/jquery/jquery-ui.min.js') }}"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.7 -->
    <script src="{{ asset('assets/plugin/bootstrap/bootstrap.min.js') }}"></script>
    <!-- daterangepicker -->
    <script src="{{ asset('assets/plugin/moment/moment.min.js') }}"></script>
    <script src="{{ asset('assets/plugin/daterangepicker/daterangepicker.js') }}"></script>
    <!-- datepicker -->
    <script src="{{ asset('assets/plugin/bootstrap-datepicker/bootstrap-datetimepicker.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('assets/js/adminlte.min.js') }}"></script>
    <!-- DataTable -->
    <script src="{{ asset('assets/plugin/datatable/datatables.min.js')}} "></script>
    <!-- CKEditor -->
    <script src="{{ asset('assets/plugin/ckeditor/ckeditor.js') }}"></script>
    <!-- Selectpicker -->
    <script src="{{ asset('assets/plugin/selectpicker/bootstrap-select.js') }}"></script>