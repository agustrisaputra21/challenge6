<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <div class="text-center">
                <h4 class="modal-title" id="myModalLabel">Delete Data</h4>
                </div>
            </div>
            <div class="modal-body pad-20">
                <p class="delete-messages">Are you sure want to delete this item(s)?</p>
            </div>
            <div class="modal-footer">
                <form id="form-delete" method='post'>
                    @method('delete')
                    @csrf
                    <input type="hidden" name="checked" id="checked">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="recoveryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <div class="text-center">
                <h4 class="modal-title" id="myModalLabel">Recovery Data</h4>
                </div>
            </div>
            <div class="modal-body pad-20">
                <p class="recovery-messages">Are you sure want to recovery this item(s)?</p>
            </div>
            <div class="modal-footer">
                <form id="form-recovery" method='post'>
                    @csrf
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Recovery</button>
                </form>
            </div>
        </div>
    </div>
</div>