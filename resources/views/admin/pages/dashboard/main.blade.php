@extends('layouts.admin')

@section('title', 'Dashboard')

@section('content-header')
    <h1>
        Dashboard
        <small>Control panel</small>
    </h1>
@endsection

@section('content')
<div class="row">
    <div class="col-xs-12"><!-- /.col-xs-12 -->
        <div class="box box-success">
            <div class="box-header with-border">
                <h1 class="font-18 m-0">Timedoor Challenge - Level 9</h1>
            </div>
            <form method="" action="">
                <div class="box-body">
                    <div class="bordered-box mb-20">
                        <form class="form" role="form" action="{{ route('admin.index') }}" method="POST">
                            @csrf
                            <table class="table table-no-border mb-0">
                                <tbody>
                                    <tr>
                                        <td width="80"><b>Title</b></td>
                                        <td>
                                            <div class="form-group mb-0">
                                                <input type="text" class="form-control" name="title" value="{{ app('request')->input('title') }}">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><b>Body</b></td>
                                        <td>
                                            <div class="form-group mb-0">
                                                <input type="text" class="form-control" name="body" value="{{ app('request')->input('body') }}">
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class="table table-search">
                                <tbody>
                                    <tr>
                                        <td width="80"><b>Image</b></td>
                                        <td width="60">
                                            <label class="radio-inline">
                                                <input type="radio" name="image" id="image1" value="1" {{ app('request')->input('image') == '1'  ? 'checked' : '' }}> with
                                            </label>
                                        </td>
                                        <td width="80">
                                            <label class="radio-inline">
                                                <input type="radio" name="image" id="image2" value="2" {{ app('request')->input('image') == '2'  ? 'checked' : '' }}> without
                                            </label>
                                        </td>
                                        <td>
                                            <label class="radio-inline">
                                                <input type="radio" name="image" id="image3" value="3" {{ app('request')->input('image') == '3' || app('request')->input('image') == '' ? 'checked' : '' }}> unspecified
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="80"><b>Status</b></td>
                                        <td>
                                            <label class="radio-inline">
                                                <input type="radio" name="status" id="inlineRadio1" value="1" {{ app('request')->input('status') == '1' ? 'checked' : '' }}> on
                                            </label>
                                        </td>
                                        <td>
                                            <label class="radio-inline">
                                                <input type="radio" name="status" id="inlineRadio2" value="2" {{ app('request')->input('status') == '2' ? 'checked' : '' }}> delete
                                            </label>
                                        </td>
                                        <td>
                                            <label class="radio-inline">
                                                <input type="radio" name="status" id="inlineRadio3" value="3" {{ app('request')->input('status') == '3' || app('request')->input('status') == '' ? 'checked' : '' }}> unspecified
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><button type="submit" class="btn btn-default mt-10"><i class="fa fa-search"></i> Search</button></td>
                                    </tr>
                                </tbody>
                            </table>
                        </form>
                    </div>
                    @if ($bulletins->isNotEmpty())
                        <table class="table table-bordered" id="data">
                            <thead>
                                <tr>
                                    <th><input type="checkbox" onchange="checkAll(this)"></th>
                                    <th>ID</th>
                                    <th>Title</th>
                                    <th>Body</th>
                                    <th width="200">Image</th>
                                    <th>Date</th>
                                    <th width="50">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($bulletins as $bulletin)
                                    @if($bulletin->trashed())
                                        <tr class="bg-gray-light">
                                            <td></td>
                                            <td>{{ $bulletin->id }}</td>
                                            <td>{{ $bulletin->title }}</td>
                                            <td>{{ $bulletin->body }}</td>
                                            <td>
                                                -
                                            </td>
                                            <td>{{ date('d-m-Y', strtotime($bulletin->created_at)) }}<br><span class="small">{{ date('H:i', strtotime($bulletin->created_at)) }}</span></td>
                                            <td><a href="{{ route('admin.recovery', $bulletin->id) }}" class="btn btn-default" rel="tooltip" title="Recover" onclick="recoveryBulletin(this)" item-name="{{ $bulletin->title}}"><i class="fa fa-repeat"></i></a></td>
                                        </tr>
                                    @else
                                        <tr>
                                            <td><input type="checkbox" value="{{ $bulletin->id }}"></td>
                                            <td>{{ $bulletin->id }}</td>
                                            <td>{{ $bulletin->title }}</td>
                                            <td>{{ $bulletin->body }}</td>
                                            <td>
                                                @if($bulletin->img)
                                                    <img class="img-prev" src="{{ asset($bulletin->imagePath) }}">
                                                    <a href="{{ route('admin.delete_image', $bulletin->id) }}" class="btn btn-danger ml-10 btn-img" rel="tooltip" onclick="deleteBulletin(this)" title="Delete Image" item-name="{{ $bulletin->title}}"><i class="fa fa-trash"></i></a>
                                                @else
                                                    -
                                                @endif
                                            </td>
                                            <td>{{ date('d-m-Y', strtotime($bulletin->created_at)) }}<br><span class="small">{{ date('H:i', strtotime($bulletin->created_at)) }}</span></td>
                                            <td><a href="{{ route('admin.delete', $bulletin->id) }}" class="btn btn-danger" rel="tooltip" title="Delete Bulletin" onclick="deleteBulletin(this)" item-name="{{ $bulletin->title}}"><i class="fa fa-trash"></i></a></td>
                                        </tr>    
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                        <a href="javascript:void(0)" class="btn btn-default mt-5" title="Delete Bulletin Checked" onclick="deleteBulletins()">Delete Checked Items</a>
                    @endif
                    <div class="text-center">
                        <nav>
                            <ul class="pagination">
                                {{ $bulletins->onEachSide(5)->links() }}
                            </ul>
                        </nav>
                    </div>
                </div>
            </form>
        </div>
    </div><!-- /.col-xs-12 -->
</div>
@endsection

@section('modal')
    @include('admin.includes.modal')
@endsection

@section('js')
<script src="{{ asset('js/admin.js') }}"></script>
<script>
    let urlDeleteChecked = "{!! route('admin.delete_all') !!}";
</script>
@endsection