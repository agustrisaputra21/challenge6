<?php

use Illuminate\Database\Seeder;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Models\Admin::insert([
            'email' => 'admin@timedoor.net',
            'name' => 'admin',
            'password' =>  bcrypt('timedoor')
        ]);
    }
}
