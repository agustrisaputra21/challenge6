<?php

use Illuminate\Database\Seeder;

class UserAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Models\User::insert([
            'name' => 'admin',
            'email' => 'admin@timedoor.net',
            'password' =>  bcrypt('timedoor'),
            'role' => 'admin'
        ]);
    }
}
