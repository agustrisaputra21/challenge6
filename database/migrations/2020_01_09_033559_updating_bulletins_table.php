<?php

use App\Models\Bulletin;

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatingBulletinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Bulletin::truncate();

        Schema::table('bulletins', function (Blueprint $table){
            $table->unsignedBigInteger('user_id')->nullable()->after('id');
            $table->string('name', 50)->nullable()->after('user_id');
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $table->dropForeign(['user_id']);
        $table->dropColumn('user_id');
        $table->dropColumn('name');
    }
}
