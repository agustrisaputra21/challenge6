<?php

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'middleware' => ['auth.admin']], function () {
    
        Route::get('/', 'DashboardController@index')->name('index');
        Route::post('{id}/recovery', 'DashboardController@restore')->name('recovery');
        Route::delete('delete_all', 'DashboardController@destroyAll')->name('delete_all');
        Route::delete('{id}/delete_image', 'DashboardController@destroyImage')->name('delete_image');
        Route::delete('{id}/destroy', 'DashboardController@destroy')->name('delete');
});