<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'BoardController@index')->middleware('auth.user');

Route::post('board', 'BoardController@store');

Route::post('board-show', 'BoardController@show')->name('show');

Route::post('board-edit', 'BoardController@update');

Route::delete('board/{id}', 'BoardController@destroy');



Auth::routes(['verify' => true]);

Route::group(['namespace' => 'Auth'], function () {
    Route::post('confirm', 'RegisterController@show')->name('confirm');
    Route::post('register', 'RegisterController@register');
    Route::get('login', 'LoginController@showLoginForm');
    Route::post('login', 'LoginController@login')->name('login');
    Route::get('logout', 'LoginController@logout')->middleware('auth:user,admin')->name('logout');
});

Route::get('verified', function(){
    return view('auth.verify-success');
})->middleware('auth');
