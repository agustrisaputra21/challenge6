function showData(data, action)
{
    let dataBulletin    = data.data;

    if (action === 'edit') {
        title       = 'Edit Data';
        nameAction  = 'Edit';
        btnAction   = 'btn-primary';
        actionClick = "editById(" + dataBulletin.id + ")";
    } else {
        title       = 'Delete data';
        nameAction  = 'Delete';
        btnAction   = 'btn-danger';
        actionClick = "deleteById(" + dataBulletin.id + ")";
    }

    $('#id').val(dataBulletin.id);
    $('#board_name_edit').val(dataBulletin.name).prop('readonly', true);
    $('#board_title_edit').val(dataBulletin.title).prop('readonly', true);
    $('#board_body_edit').val(dataBulletin.body).prop('readonly', true);
    $('#delete-warning').hide();
    $('#bulletinForm').show();

    if (dataBulletin.img !== null) {
        $('#board_image').html(`
            <img class="img-responsive" alt="" src="${dataBulletin.imagePath}">
        `);
    } else {
        $('#board_image').html(`
            <img class="img-responsive" alt="" src="http://via.placeholder.com/300x300">
        `);
    }

    if (data.success) {
        action === 'edit' ? $('#board_edit_img').show() : $('#board_edit_img').hide() + $('#delete-warning').show();

        if (action === 'edit') {
            $('#board_edit_img').show();
            $('#board_name_edit').prop('readonly', false);
            $('#board_title_edit').prop('readonly', false);
            $('#board_body_edit').prop('readonly', false);
        } else {
            $('#bulletinForm').hide();
            $('#board_edit_img').hide();
            $('#delete-warning').show();
        }
    
        $('#delete-danger').hide();

        $('#pwd').html(``);

        $('.modal-footer').html(`
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="button" class="btn ${btnAction}" onclick=${actionClick}>${nameAction}</button>
        `);
    } else {
        $('#board_edit_img').hide();
        $('#delete-danger').show();

        if(data.state === 1) {
            $('#error_msg').text('The passwords you entered do not match. Please try again.');
            $('#pwd').html(`
                <label>Password</label>
                <input type="password" name="password" id="password" class="form-control">
            `);
            $('#pwd').show();
            $('.modal-footer').html(`
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn ${btnAction}" onclick="tryAgain(${dataBulletin.id}, '${action}')">${nameAction}</button>
            `);
        } else {
            $('#error_msg').text('This message can’t ' + action + ', because this message has not been set password.');
            $('#pwd').html(``);
            $('#pwd').hide();
            $('.modal-footer').html(`
                <button type="button" class="btn btn-default" data-dismiss="modal">Back</button>
            `);
        }
    }

    $('.modal-title').text(title);
    $('#boardModal').modal('show');
}

function deleteBulletin(id) {
    $.ajax({
        data: $('#bulletinForm'+id).serialize(),
        url: "board-show",
        type: "post",
        dataType: 'json',
        success: function (data) {
            $('input[name=password]').val('');
            showData(data, 'delete');
        },
        error: function (data) {
        console.log('Error:', data);
        }
    });
}

function editBulletin(id) {
    $.ajax({
        data: $('#bulletinForm'+id).serialize(),
        url: "board-show",
        type: "post",
        dataType: 'json',
        success: function (data) {
            $('input[name=password]').val('');
            showData(data, 'edit');
        },
        error: function (data) {
            console.log('Error:', data);
        }
    });
}

function tryAgain(id, action) {
    $.ajax({
        data: $('#bulletinForm').serialize(),
        url: "board-show",
        type: "post",
        dataType: 'json',
        success: function (data) {
            $('input[name=password]').val('');
            showData(data, action)
        },
        error: function (data) {
            console.log('Error:', data);
        }
    });
}