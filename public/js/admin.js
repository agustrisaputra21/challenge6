let grid = document.getElementById("data");
let checkBoxes = grid.getElementsByTagName("input");

function checkAll(el)
{
    for (let i = 0; i < checkBoxes.length; i++) {
        if (el.checked) {
            checkBoxes[i].checked = true;
        } else {
            checkBoxes[i].checked = false;
        }
    }   
}

function deleteBulletins() 
{
    event.preventDefault();

    let data = "";
    let id = [];

    for (let i = 0; i < checkBoxes.length; i++) {
        data = checkBoxes[i]
        if (data.checked) {
            if(data.getAttribute('value') !== null) {
                id.push(data.getAttribute('value'))
            }
        }
    }
    
    let url = urlDeleteChecked,
        title = 'Delete All Data',
        form = document.getElementById('form-delete'),
        input = document.getElementById('checked')
        modalTitle = document.getElementById('myModalLabel',)
        body = document.getElementsByClassName('delete-messages');

        form.setAttribute('action', url)
        input.value = id;
        modalTitle.innerHTML = title;
        body[0].innerHTML = `Are you sure want to delete all data checked?`
        $('#deleteModal').modal('show');
}

function deleteBulletin(el)
{
    event.preventDefault();

    let me          = el,
        url         = me.getAttribute('href'),
        title       = me.getAttribute('title'),
        item        = me.getAttribute('item-name'),
        form        = document.getElementById('form-delete'),
        modalTitle  = document.getElementById('myModalLabel',)
        body        = document.getElementsByClassName('delete-messages');

        form.setAttribute('action', url);
        modalTitle.innerHTML = title;
        body[0].innerHTML = `Are you sure want to delete <b>"${item}"</b>?`;
        $('#deleteModal').modal('show');
}

function recoveryBulletin(el)
{
    event.preventDefault();

    let me          = el,
        url         = me.getAttribute('href'),
        title       = me.getAttribute('title'),
        item        = me.getAttribute('item-name'),
        form        = document.getElementById('form-recovery'),
        modalTitle  = document.getElementById('myModalLabel',)
        body        = document.getElementsByClassName('recovery-messages');

        form.setAttribute('action', url);
        modalTitle.innerHTML = title;
        body[0].innerHTML = `Are you sure want to recovery <b>"${item}"</b>?`;
        $('#recoveryModal').modal('show');
}