<?php

namespace App\Traits;

use Illuminate\Support\Facades\Storage;

trait DeleteTrait {
    public function deleteImg($disk = 'public', $file = null, $folder = null) {
        $originalPath = $folder . '/' .$file;
        $thumbnailPath = $folder . '/' . 'thumbnail/' . $file;

        Storage::disk($disk)->delete($thumbnailPath);

        $file = Storage::disk($disk)->delete($originalPath);
        
        return $file;
        
    }
}