<?php

namespace App\Traits;

use App\Models\Bulletin as bulletin;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Image;

trait UploadTrait {
    public function uploadImg(UploadedFile $file, $folder = null, $disk = 'public', $fileName = null) {
        $name = !is_null($fileName) ? $fileName : Str::random(25);

        $destinationPath = $folder . '/thumbnail';

        $resize_image = Image::make($file->getRealPath());

        $resize_image->resize(100, 100, function($constraint){
        $constraint->aspectRatio();
        });
        
        $resize_image->stream();

        $filePath = $destinationPath . '/' . $name . '.' . $file->getClientOriginalExtension();

        Storage::disk($disk)->put($filePath, $resize_image, $disk);
        
        $file->storeAs($folder, $name . '.' . $file->getClientOriginalExtension(), $disk);

        return $name . '.' . $file->getClientOriginalExtension();
    }
}