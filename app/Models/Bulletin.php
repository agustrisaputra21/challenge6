<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bulletin extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'user_id', 'name', 'title', 'body', 'img'
    ];

    protected $hidden = [
        'password'
    ];

    public static function boot()
    {
        parent::boot();

        static::addGlobalScope('order', function (Builder $builder) {
            $builder->orderBy('id', 'DESC');
        });
    }

    public function scopeSearch($query, $request)
    {
        if (!empty($request->title)) {
            $query->where('title','like', "%$request->title%");
        }

        if (!empty($request->body)) {
            $query->where('body', 'like', "%$request->body%");
        }

        switch ($request->image) {
            case '1':
                $query->whereNotNull('img');
                break;
            case '2':
                $query->whereNull('img');
                break;
        }

        switch ($request->status) {
            case '1':
                $query->withoutTrashed();
                break;
            
            case '2':
                $query->onlyTrashed();
                break;
            
            default:
            $query->withTrashed();
                break;
        }
    }

    public $imgDirectory = 'bulletin';

    protected function getImagePathAttribute($value)
    {
        return Storage::url($this->imgDirectory . '/' . 'thumbnail' . '/' . $this->attributes['img']);
    }

    protected $appends = ['imagePath'];

    public function user() {
        return $this->belongsTo('App\Models\User');
    }
}
