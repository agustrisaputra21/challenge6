<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BulletinFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'board_name'    => 'nullable|min:3|max:16',
            'board_title'   => 'required|min:10|max:32',
            'board_body'    => 'required|min:10|max:200',
            'board_img'     => 'nullable|file|image|mimes:jpeg,jpg,png,gif|max:1024',
            'password'      => 'nullable|digits:4'
        ];
    }

    public function messages() {
        return [
            'board_name.min'        => 'Your name must be 3 to 16 characters long',
            'board_name.max'        => 'Your name must be 3 to 16 characters long',
            'board_title.required'  => 'Your title must be filled in.',
            'board_title.min'       => 'Your title must be 10 to 32 characters long.',
            'board_title.max'       => 'Your title must be 10 to 32 characters long.',
            'board_body.required'   => 'Your body must be filled in.',
            'board_body.min'        => 'Your body must be 10 to 200 characters long.',
            'board_body.max'        => 'Your body must be 10 to 200 characters long.',
            'board_img.mimes'       => 'Your image is only valid .jpeg, .jpg, .png, .gif',
            'board_img.max'         => 'Your image is only valid 1MB or less',
            'password.digits'       => 'Your password must be 4 digit number.'
        ];
    }
}
