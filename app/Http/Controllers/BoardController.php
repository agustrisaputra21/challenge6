<?php

namespace App\Http\Controllers;

use App\Models\Bulletin as bulletin;
use App\Traits\UploadTrait as upload;
use App\Traits\DeleteTrait as delete;

use Illuminate\Http\Request;
use App\Http\Requests\BulletinFormRequest;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

use Illuminate\Support\Facades\Auth;

class BoardController extends Controller
{
    use upload;
    use delete;

    public function index() {
        $boards = bulletin::paginate(10);
        
        return view('bulletin.index', ['boards' => $boards]);
    }

    public function store(BulletinFormRequest $request) {
        $bulletin = new bulletin;

        $bulletin->user_id  = Auth::id();
        $bulletin->name     = $request->get('board_name');
        $bulletin->title    = $request->get('board_title');
        $bulletin->body     = $request->get('board_body');

        if ($request->hasFile('board_img')) {
            $image          = $request->file('board_img');
            $imageName      = Str::slug($bulletin->title)
                            . '_' 
                            . time();
            $folder         = $bulletin->imgDirectory;
            $filePath       = $this->uploadImg($image, $folder, 'public', $imageName);
            $bulletin->img  = $filePath;
        }

        $bulletin->password =  $request->get('password') ?
                                \Hash::make( $request->get('password') ) :
                                null;

        $bulletin->save();

        return redirect()->back();
    }

    public function show(Request $request) {
        $password =  $request->get('password');
    
        $bulletin = bulletin::where('id', $request->get('id'))->first();

        if (Auth::user()) {
            return response()->json([
                                        'success'   => true, 
                                        'data'      => $bulletin
                                    ]);
        } else {
            if (!is_null($bulletin->password)) {
                if (Hash::check($password, $bulletin->password)) {
                    return response()->json([
                                                'success'   => true, 
                                                'data'      => $bulletin
                                            ]);
                } else {
                    return response()->json([
                                                'success'   => false, 
                                                'state'     => 1,  
                                                'data'      => $bulletin
                                            ]);
                }
            } else {
                return response()->json([
                                            'success'   => false, 
                                            'state'     => 2, 
                                            'data'      => $bulletin
                                        ]);
            }
        }
    }

    public function update(BulletinFormRequest $request) {
        $imgChecked         = json_decode( $request->get('img_checked') );
        $bulletin           = bulletin::find($request->get('id'));
        
        $bulletin->name     = $request->get('board_name');
        $bulletin->title    = $request->get('board_title');
        $bulletin->body     = $request->get('board_body');
        
        if ($request->hasFile('board_img')) {
            $this->deleteImg('public', $bulletin->img, $bulletin->imgDirectory);

            $image          = $request->file('board_img');

            $imageName      = Str::slug($bulletin->title)
                            . '_' 
                            . time();

            $folder         = $bulletin->imgDirectory;

            $filePath       = $this->uploadImg($image, $folder, 'public', $imageName);

            $bulletin->img  = $filePath;
        }

        if ($imgChecked && !is_null($bulletin->img)) {
            $this->deleteImg('public', $bulletin->img, $bulletin->imgDirectory);
      
            $bulletin->img = null;
        }

        $bulletin->save();

        return response()->json([
                                    'success' => true, 
                                    'message' => 'Bulletin deleted successfully'
                                ]);
    }

    public function destroy($id) {
        $bulletin = bulletin::findOrFail($id);

        if (!is_null($bulletin->img)) {
            $this->deleteImg('public', $bulletin->img, $bulletin->imgDirectory);
        }

        $bulletin->delete();

        return response()->json([
                                    'success' => true, 
                                    'message' => 'Bulletin deleted successfully'
                                ]);
    }
}
