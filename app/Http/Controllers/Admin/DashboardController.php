<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Bulletin;
use App\Traits\DeleteTrait as delete;

class DashboardController extends Controller
{
    use delete;

    public function index(Request $request)
    {
        $bulletin = new Bulletin;

        $bulletins = $bulletin->search($request)
                                ->paginate(20);
                                
        return view('admin.pages.dashboard.main', compact('bulletins'));
    }

    public function restore($id)
    {
        $bulletin = bulletin::withTrashed()->find($id)->restore();

        return back()->with('success', 'Bulletin has been restore');
    }

    public function destroyImage($id)
    {
        $bulletin = bulletin::findOrFail($id);

        if (!is_null($bulletin->img)) {
            $this->deleteImg('public', $bulletin->img, $bulletin->imgDirectory);

            $bulletin->img = null;
        }

        $bulletin->save();

        return back()->with('success', 'Image has been deleted');
    }

    public function destroyAll(Request $request)
    {
        $id = explode(',', $request->checked);
        
        $bulletins = bulletin::whereIn('id', $id)->get();
        
        try {
            foreach ($bulletins as $bulletin) {
                if (!is_null($bulletin->img)) {
                    $this->deleteImg('public', $bulletin->img, $bulletin->imgDirectory);
                }
    
                $bulletin->delete();
            }

            $message = ['success' => 'Bulletin has been deleted'];
        } catch (\Throwable $e) {
            $message = ['error' => $e->getMessage()];
        }

        return back()->with($message);
    }

    public function destroy($id)
    {
        $bulletin = bulletin::findOrFail($id);

        if (!is_null($bulletin->img)) {
            $this->deleteImg('public', $bulletin->img, $bulletin->imgDirectory);
            $bulletin->img = null;
            $bulletin->update();
        }

        $bulletin->delete();

        return back()->with('success', 'Bulletin has been deleted');
    }
}
