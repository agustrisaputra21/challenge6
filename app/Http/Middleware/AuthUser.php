<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Illuminate\Support\Facades\Session;

class AuthUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $this->auth = true;

        if (auth()->user()) {
            $this->auth = auth()->user()->role == 'user' ? true : false;
        }

        if (!$this->auth) {
            return redirect('admin');
        }

        return $next($request);
    }
}
